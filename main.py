import networkx as nx
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os.path

# G = nx.Graph()
# e = [('a', 'b', 0.3), ('b', 'c', 0.9), ('a', 'c', 0.5), ('c', 'd', 1.2)]
# G.add_weighted_edges_from(e)
# print(nx.dijkstra_path(G, 'a', 'd'))

# check if can find the nodes.txt file
if not os.path.exists("nodes.txt"):
    print("Error! File \"nodes.txt\" not found!")
    while True:
        pass

arc_weight = ''
# import the nodes.txt as a weighted graph
try:
    G = nx.read_weighted_edgelist("nodes.txt", delimiter=",")
    # The edge weights of each arcs are stored in a dictionary
    arc_weight = nx.get_edge_attributes(G, 'weight')
    #print(arc_weight)
    print("File \"nodes.txt\" imported with success!")
except:
    print("Error! File \"nodes.txt\" with wrong format!")


f = plt.figure()

pos = nx.spring_layout(G)
#nx.draw(G, pos=pos, node_color='black', edge_color='k', node_label=True, node_size=250)
#nx.draw_networkx_edge_labels(G, pos, font_size=7, alpha=0.7, edge_labels=arc_weight)  # use default edge labels
#nx.draw_networkx_labels(G, pos)  # use default node labels

# Draw the nodes
nx.draw_networkx(G, pos=pos, node_size=250, node_color="black", font_size=7, label="Graph")
# Draw the node labels
nx.draw_networkx_labels(G, pos=pos, font_color="white")
# Draw the edges
nx.draw_networkx_edges(G, pos=pos, font_size=7, edge_color="black", alpha=0.7)
# Draw the edge labels
nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=arc_weight)

f.savefig("graph.png")


while True:
    f = plt.figure(222)
    print(" ")
    print("**")
    print("Dijkstra path")
    #try:
    startPoint, endPoint = input('Please enter start and end points (with a comma in between): ').split(',')
    sp = nx.dijkstra_path(G, startPoint, endPoint)
    # Create a list of arcs in the shortest path using the zip command and store it in red edges
    red_edges = list(zip(sp, sp[1:]))
    # If the node is in the shortest path, set it to red, else set it to white color
    node_col = ['black' if not node in sp else 'red' for node in G.nodes()]
    # node font collor
#    node_col_font = ['blue' if not node in sp else 'black' for node in G.nodes()]
#    print(node_col_font)
    # If the edge is in the shortest path set it to red, else set it to white color
    edge_col = ['black' if not edge in red_edges else 'red' for edge in G.edges()]
    # Draw the nodes
    nx.draw_networkx(G, pos=pos, node_color=node_col, node_size=250, font_size=7, label="Graph", with_labels=True)
    # Draw the node labels
#    nx.draw_networkx_nodes(G, pos=pos, node_color=node_col, font_color=node_col_font, font_size=7)
    nx.draw_networkx_nodes(G, pos=pos, node_color=node_col, font_size=7)
    nx.draw_networkx_labels(G, pos=pos, font_color="white")
    # Draw the edges
    nx.draw_networkx_edges(G, pos=pos, edge_color=edge_col, font_size=7, alpha=0.7)
    # Draw the edge labels
    nx.draw_networkx_edge_labels(G, pos=pos, edge_color=edge_col, edge_labels=arc_weight)
    f.savefig("graph.png")
    print("Solution: " + str(sp))
    #except:
    #    print("Error! Wrong input format. Please enter start and end points split by a comma (Ex. 1,2)")
